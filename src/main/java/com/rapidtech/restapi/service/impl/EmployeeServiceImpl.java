package com.rapidtech.restapi.service.impl;

import com.rapidtech.restapi.model.EmployeeModel;
import com.rapidtech.restapi.repository.EmployeeRepo;
import com.rapidtech.restapi.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepo repo;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepo repo) {
        this.repo = repo;
    }

    @Override
    public List<EmployeeModel> getAll() {
        return null;
    }

    @Override
    public Optional<EmployeeModel> getById(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<EmployeeModel> save(EmployeeModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<EmployeeModel> update(Long id, EmployeeModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<EmployeeModel> delete(Long id) {
        return Optional.empty();
    }
}
