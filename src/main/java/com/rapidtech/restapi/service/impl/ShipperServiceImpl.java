package com.rapidtech.restapi.service.impl;

import com.rapidtech.restapi.entity.ProductEntity;
import com.rapidtech.restapi.model.CustomerModel;
import com.rapidtech.restapi.model.ProductModel;
import com.rapidtech.restapi.model.ShipperModel;
import com.rapidtech.restapi.repository.ProductRepo;
import com.rapidtech.restapi.repository.ShipperRepo;
import com.rapidtech.restapi.service.ProductService;
import com.rapidtech.restapi.service.ShipperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ShipperServiceImpl implements ShipperService {
    private ShipperRepo repo;

    @Autowired
    public ShipperServiceImpl(ShipperRepo repo) {
        this.repo = repo;
    }


    @Override
    public List<ShipperModel> getAll() {
        return this.repo.findAll().stream().map(ShipperModel::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ShipperModel> getById(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<ShipperModel> save(ShipperModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<ShipperModel> update(Long id, ShipperModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<ShipperModel> delete(Long id) {
        return Optional.empty();
    }
}
