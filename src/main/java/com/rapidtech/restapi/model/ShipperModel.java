package com.rapidtech.restapi.model;

import com.rapidtech.restapi.entity.CustomerEntity;
import com.rapidtech.restapi.entity.ShipperEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShipperModel {
    private Long id;
    private String shipperName;
    private String phone;

    public ShipperModel(ShipperEntity shipperEntity){
        BeanUtils.copyProperties(shipperEntity, this);
    }
}
