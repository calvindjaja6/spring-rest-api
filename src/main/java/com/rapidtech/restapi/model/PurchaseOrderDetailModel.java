package com.rapidtech.restapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrderDetailModel {
    private Long id;
    private Long poId;
    private Long productId;
    private Double quantity;
    private Double price;
}
